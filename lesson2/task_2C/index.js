const express = require('express');
const cors = require('cors');

const __DEV__ = false;

const canon = require('./canonize');

const app = express();
app.use(cors());

app.get('/', (req, res) => {
	const userName = req.query.username;
	let result = canon.start(userName);

	return res.send(result);
});

app.listen(8888, () => {
	if(__DEV__) {
		console.log('listen start');
	}
});