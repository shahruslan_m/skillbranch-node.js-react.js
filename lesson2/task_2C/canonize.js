function canonize(url) {

	if(typeof(url) == 'string') {
		const re = new RegExp('(https?)?:?((\/\/)?(.*?)[-_a-zA-z0-9]+\.[-_a-zA-z0-9]+\/)?@?([-._a-zA-z0-9]+)');
		const userName = url.match(re);

		if(userName != null) {
			return `@${userName[5]}`;
		}
	}
	
	return 'Invalid username';
}

module.exports.start = canonize;