const express = require('express');

const app = express();


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
	let {a = 0, b = 0} = req.query;

	a = parseInt(a);
	b = parseInt(b);
	
	if(typeof(a) != 'number') {
		a = 0;
	}
	if(typeof(b) != 'number') {
		b = 0;
	}

	const c = `${(a + b)}`;

	return res.send(c);
});

app.listen(8888);