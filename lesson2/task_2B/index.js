const express = require('express');
const cors = require('cors');

const __DEV__ = true;

const app = express();
app.use(cors());

const re = /[-0-9_\\\/]/

function formatName(name) {
	return ` ${name[0].toUpperCase()}.`;
}

function formatMiddleName(name) {
	return `${name[0].toUpperCase() + name.substring(1).toLowerCase()}`
}

app.get('/', (req, res) => {

	let result = 'Invalid fullname';
	const fullName = req.query.fullname;

	if(fullName != undefined && fullName.length > 0) {
		let words = fullName.split(' ');

		let bError = false;
		words.forEach((word) => {
			if(re.test(word)) {
				bError = true;
			}
		});

		if(bError) {
			words = [];
		}

		words = words.filter((word) => {
			return word.length > 0;
		});

		if(words.length == 1) {
			result = formatMiddleName(words[0]);
		} else if(words.length == 2) {
			result = formatMiddleName(words[1]) + formatName(words[0]);
		} else if(words.length == 3) {
			result = formatMiddleName(words[2]) + formatName(words[0]) + formatName(words[1]);
		}

	}

	return res.send(result);
});

app.listen(8888, () => {
	if(__DEV__) {
		console.log('Start listen 8888');
	}
});