import express from 'express';
import cors from 'cors';
import Color from 'color';

const app = express();

app.use(cors());

function hex(req, res, next) {
	const paramColor = req.query.color;

	if(paramColor != undefined) {
		const re = /^\#?(([0-9a-f]{3}){1,2})$/;
		const matches = paramColor.toLowerCase().trim().match(re);
		
		if(matches != null) {
			let str = '';
			const color = matches[1];

			for(const key in color) {
				const char = color[key];
				
				str += char;
				if(color.length == 3) {
					str += char;
				}			
			}

			return res.send('#'+str);
		}
	}
	
	next();
}

function rgb(req, res, next) {
	const paramColor = req.query.color;

	if(paramColor != undefined) {
		const re = /^rgb\((\s*\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/;
		const color = paramColor.toLowerCase().trim();

		const matches = color.match(re);
		if(matches != null) {
			const r = parseInt(matches[1]);
			const g = parseInt(matches[2]);
			const b = parseInt(matches[3]);

			if(r <= 255 && g <= 255 && b <= 255) {
				let hexR = r.toString(16);
				let hexG = g.toString(16);
				let hexB = b.toString(16);

				if(hexR.length == 1) {
					hexR = '0'+hexR;
				}
				if(hexG.length == 1) {
					hexG = '0'+hexG;
				}
				if(hexB.length == 1) {
					hexB = '0'+hexB;
				}

				const str = '#' + hexR + hexG + hexB;

				return res.send(str);
			}
		}

		
	}
	
	next();
}

function hsl(req, res, next) {
	const paramColor = req.query.color;

	if(paramColor != undefined) {
		const re = /^hsl\((\s*\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*\)$/;
		const color = paramColor.toLowerCase().trim();

		console.log(color);

		const matches = color.match(re);
		if(re.test(color)) {
			const c = new Color(color);
			return res.send(c.hexString());
		}

		
	}
	
	next();
}



app.use(hex);
app.use(rgb);
app.use(hsl);


app.get('/', (req, res)=>{
	let returnStr = 'Invalid color';
	const paramColor = req.query.color;
	const re = /^\#?([0-9a-f]{3}){1,2}$/;


	if(paramColor != undefined) {
		let color = paramColor.toLowerCase().trim();
		

		if(re.test(color)) {
			let str = '';
			color = color.replace('#', '');

			for(const key in color) {
				str += color[key];
				if(color.length == 3) {
					str += color[key];
				}			
			}

			returnStr = '#'+str;
		}
	}

	
	
	res.send(returnStr);
});

app.listen(8888, ()=>{
	console.log('start listen');
});