import express from 'express';
import fetch from 'isomorphic-fetch';
import cors from 'cors';
import promise from 'bluebird';
import _ from 'lodash';

const app = express();
app.use(cors());

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
let pc = {};
let ret = '';

const getPcInfo = (req, res, next) => {

	const url = req._parsedUrl.pathname;
	const parts = _.compact(url.split('/'));

	let bNotFound = false;
	let current = pc;

	parts.forEach((val) => {
		if(current[val] === undefined || !_.isPlainObject(current) && val=='length') {
			bNotFound = true;
		}

		if(!bNotFound) {
			current = current[val];
		}
	});

	if(bNotFound) {
		res.status(404).send('Not Found');
	} else {
		res.json(current);
	}
}

const getPcAllInfo = (req, res, next) => {
	res.json(pc);
}

const getPcVolumes = (res, req, next) => {
	const result = {};
	
	pc.hdd.forEach((val) => {
		const volume = val.volume;
		if(result[volume] === undefined) {
			result[volume] = val.size;
		} else {
			result[volume] += val.size;
		}
	});

	for(const volume in result) {
		result[volume] += "B";
	}

	req.json(result);
}

app.get('/volumes', getPcVolumes);
app.get('/', getPcAllInfo);
app.get(/\/.+/, getPcInfo);

app.listen(8888, async () => {
	try {
		const responsePc = await fetch(pcUrl);
		pc = await responsePc.json();

		console.log('Expample app listing on on port 8888');
	} catch (err) {
		console.log('Что-то пошло не так:', err);
	}
});